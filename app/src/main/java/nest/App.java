package nest;

import android.app.Application;

import nest.dagger.DaggerGraph;
import nest.utils.TraceTree;
import timber.log.Timber;

public class App extends Application {

    private static DaggerGraph daggerGraph;

    public static DaggerGraph getGraph() {
        return daggerGraph;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        daggerGraph = DaggerGraph.Initializer.init(this);
        daggerGraph.inject(this);

        Timber.plant(new TraceTree(true));
    }
}
