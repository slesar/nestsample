package nest.ui.holder;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.inject.Inject;

import nest.App;
import nest.R;
import nest.model.nest.AwayState;
import nest.model.nest.Structure;
import nest.service.nest.NestService;

public class StructureViewHolder implements View.OnClickListener {

    @Inject
    NestService nestService;

    final View rootView;

    final Button homeButton;
    final TextView titleText;
    final Button logoutButton;

    private Structure structure;

    public StructureViewHolder(View view) {
        App.getGraph().inject(this);

        this.rootView = view;

        homeButton = (Button) view.findViewById(R.id.nest_home_button);
        homeButton.setOnClickListener(this);
        titleText = (TextView) view.findViewById(R.id.nest_title);
        logoutButton = (Button) view.findViewById(R.id.nest_logout_button);
        logoutButton.setOnClickListener(this);
    }

    public void updateStructure(Structure structure) {
        this.structure = structure;

        homeButton.setText(structure.awayState == AwayState.AWAY || structure.awayState == AwayState.AUTO_AWAY
                ? R.string.structure_state_away
                : R.string.structure_state_home);
        titleText.setText(structure.name);
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();
        switch (id) {
            case R.id.nest_home_button:
                toggleAway();
                break;
            case R.id.nest_logout_button:
                nestService.logout();
                break;
        }
    }

    public void setAway() {
        structure.awayState = AwayState.AWAY;
        homeButton.setText(R.string.structure_state_away);
        nestService.sendRequest(structure.getPath(Structure.FIELD_AWAY), AwayState.AWAY.getKey());
    }

    public void setHome() {
        structure.awayState = AwayState.HOME;
        homeButton.setText(R.string.structure_state_home);
        nestService.sendRequest(structure.getPath(Structure.FIELD_AWAY), AwayState.HOME.getKey());
    }

    public void toggleAway() {
        if (structure != null) {
            switch (structure.awayState) {
                case AUTO_AWAY:
                case AWAY:
                    setHome();
                    break;
                case HOME:
                default:
                    setAway();
                    break;
            }
        }
    }
}
