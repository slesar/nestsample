package nest.ui.holder;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import nest.App;
import nest.R;
import nest.model.nest.AwayState;
import nest.model.nest.HvacMode;
import nest.model.nest.Thermostat;
import nest.service.nest.NestService;
import nest.ui.view.ThermostatView;

public class ThermostatViewHolder implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    @Inject
    NestService nestService;

    // TODO range_control
    // TODO temp_cool_up
    // TODO temp_cool_down
    // TODO temp_heat_up
    // TODO temp_heat_down

    final View rootView;

    final ViewGroup complexIndicator;
    final ThermostatView thermostatIndicator;
    final TextView temperatureText;
    final View fanIndicator;
    final View tempUp;
    final View tempDown;
    final TextView humidityText;
    final TextView outsideText;
    final Spinner thermostatControl;
    final View progress;

    private ArrayList<HvacMode> thermostatModes;

    private Thermostat thermostat;

    private final Resources res;

    private AwayState currentState;

    public ThermostatViewHolder(View view) {
        App.getGraph().inject(this);

        rootView = view;
        res = view.getResources();

        progress = view.findViewById(R.id.nest_progress);

        complexIndicator = (ViewGroup) view.findViewById(R.id.nest_complex_indicator);
        thermostatIndicator = (ThermostatView) view.findViewById(R.id.nest_indicator);
        temperatureText = (TextView) view.findViewById(R.id.nest_temperature);
        fanIndicator = view.findViewById(R.id.nest_fan_indicator);
        tempUp = view.findViewById(R.id.nest_temp_up);
        tempUp.setOnClickListener(this);
        tempDown = view.findViewById(R.id.nest_temp_down);
        tempDown.setOnClickListener(this);

        humidityText = (TextView) view.findViewById(R.id.nest_humidity);
        // TODO
        outsideText = (TextView) view.findViewById(R.id.nest_outside);

        thermostatControl = (Spinner) view.findViewById(R.id.nest_thermostat_control);
        resetControlListener();
    }

    public void updateThermostat(Thermostat thermostat) {
        hideProgress();

        this.thermostat = thermostat;

        if (!isOnline() || thermostat.hvacMode == HvacMode.OFF) {
            temperatureText.setText(R.string.thermostat_off);
            thermostatIndicator.setEnabled(false);
        } else {
            temperatureText.setText(res.getString(R.string.thermostat_ambient_temperature, thermostat.targetTemperatureC));
            thermostatIndicator.setEnabled(true);
        }

        if (isOnline() && thermostat.humidity > 0) {
            humidityText.setText(res.getString(R.string.thermostat_humidity, thermostat.humidity));
        } else {
            humidityText.setText(null);
        }

        if (!isOnline()) {
            return;
        }

        switch (thermostat.hvacMode) {
            case COOL:
            case HEAT:
                updateSingle();
                break;
            case HEAT_AND_COOL:
                updateRange();
                break;
        }

        updateModes();
    }

    public void setEnabled(boolean value) {
        thermostatIndicator.setEnabled(value);
    }

    public void setAwayState(AwayState state) {
        if (currentState != state) {
            currentState = state;
            setEnabled(isOnline() && thermostat.hvacMode != HvacMode.OFF);
            if (thermostat != null) {
                updateThermostat(thermostat);
            }
        }
    }

    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    private void updateModes() {
        resetControlListener();

        if (thermostatModes == null) {
            ArrayList<String> modes = new ArrayList<>();
            thermostatModes = new ArrayList<>();
            if (thermostat.canCool) {
                modes.add(res.getString(R.string.thermostat_mode_cool));
                thermostatModes.add(HvacMode.COOL);
            }
            if (thermostat.canHeat) {
                modes.add(res.getString(R.string.thermostat_mode_heat));
                thermostatModes.add(HvacMode.HEAT);
            }
            if (thermostat.canCool && thermostat.canHeat) {
                modes.add(res.getString(R.string.thermostat_mode_heat_cool));
                thermostatModes.add(HvacMode.HEAT_AND_COOL);
            }
            modes.add(res.getString(R.string.thermostat_mode_off));
            thermostatModes.add(HvacMode.OFF);
            final ArrayAdapter<String> adapter = new ArrayAdapter<>(rootView.getContext(), android.R.layout.simple_spinner_dropdown_item, modes);
            thermostatControl.setAdapter(adapter);
        }

        int selectedPos = 0;
        for (int i = 0; i < thermostatModes.size(); i++) {
            if (thermostatModes.get(i) == thermostat.hvacMode) {
                selectedPos = i;
                break;
            }
        }
        thermostatControl.setSelection(selectedPos);
    }

    private void updateSingle() {
        if (thermostat.hasFan && thermostat.fanTimerActive) {
            fanIndicator.setVisibility(View.VISIBLE);
        } else {
            fanIndicator.setVisibility(View.GONE);
        }
        thermostatIndicator.setTargetTemperature((float) thermostat.targetTemperatureC);
        if (thermostat.hvacMode == HvacMode.COOL) {
            thermostatIndicator.setColorTarget(res.getColor(R.color.thermostat_target_cool));
        } else {
            thermostatIndicator.setColorTarget(res.getColor(R.color.thermostat_target_heat));
        }
        thermostatIndicator.setCurrentTemperature((float) thermostat.ambientTemperatureC);
        if (thermostat.ambientTemperatureC > 24) {
            thermostatIndicator.setColorCurrent(res.getColor(R.color.thermostat_current_warm));
        } else {
            thermostatIndicator.setColorCurrent(res.getColor(R.color.thermostat_current_cold));
        }
    }

    private void updateRange() {
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();
        switch (id) {
            case R.id.nest_temp_up:
                setTempUp();
                break;
            case R.id.nest_temp_down:
                setTempDown();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.nest_thermostat_control: {
                if (isOnline()) {
                    nestService.sendRequest(thermostat.getPath(Thermostat.FIELD_HVAC_MODE), thermostatModes.get(position).getKey());
                }
            }
            break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void resetControlListener() {
        thermostatControl.setOnItemSelectedListener(null);
        thermostatControl.postDelayed(() -> thermostatControl.setOnItemSelectedListener(ThermostatViewHolder.this), 100);
    }

    public void setTempUp() {
        setTemp(.5F);
    }

    public void setTempDown() {
        setTemp(-.5F);
    }

    private void setTemp(float value) {
        if (isOnline() && (thermostat.hvacMode == HvacMode.COOL || thermostat.hvacMode == HvacMode.HEAT)) {
            double temp = thermostat.targetTemperatureC + value;
            temp = Math.min(32, temp);
            temp = Math.max(9, temp);
            thermostat.targetTemperatureC = temp;
            thermostatIndicator.setTargetTemperature((float) temp);
            nestService.sendRequest(thermostat.getPath(Thermostat.FIELD_TARGET_TEMP_C), temp);
        }
    }

    private boolean isOnline() {
        return thermostat != null && thermostat.isOnline && currentState == AwayState.HOME;
    }
}
