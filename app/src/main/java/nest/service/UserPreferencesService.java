package nest.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import nest.model.nest.AccessToken;

public class UserPreferencesService {

    private static final String NEST_TOKEN_KEY = "NEST_TOKEN_KEY";
    private static final String NEST_TOKEN_EXPIRATION = "NEST_TOKEN_EXPIRATION";

    private final Context context;

    private SharedPreferences prefs;

    public UserPreferencesService(Context context) {
        this.context = context;
    }

    public AccessToken getNestToken() {
        final String token = getPrefs().getString(NEST_TOKEN_KEY, null);
        final long expirationDate = getPrefs().getLong(NEST_TOKEN_EXPIRATION, -1);

        if(token == null || expirationDate == -1) {
            return null;
        }

        return new AccessToken(token, expirationDate);
    }

    public void setNestToken(AccessToken token) {
        if (token == null) {
            getPrefs().edit()
                    .remove(NEST_TOKEN_KEY)
                    .remove(NEST_TOKEN_EXPIRATION)
                    .apply();
        } else {
            getPrefs().edit()
                    .putString(NEST_TOKEN_KEY, token.token)
                    .putLong(NEST_TOKEN_EXPIRATION, token.expiresIn)
                    .apply();
        }
    }

    SharedPreferences getPrefs() {
        if (prefs == null) {
            prefs = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return prefs;
    }
}
