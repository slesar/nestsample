package nest.service.nest;

import nest.model.nest.ClientMetadata;

public interface NestListener {

    void onStartActivityForResult(int code, ClientMetadata data);

    void onAuthStatusChanged(boolean isAuth);

    void onLoggedOut();
}
