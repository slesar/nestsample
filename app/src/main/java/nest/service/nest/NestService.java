package nest.service.nest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.WorkerThread;

import com.bluelinelabs.logansquare.LoganSquare;
import com.firebase.client.Config;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.GenericTypeIndicator;
import com.firebase.client.Logger;
import com.firebase.client.ValueEventListener;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

import nest.App;
import nest.BuildConfig;
import nest.activity.NestAuthActivity;
import nest.model.nest.AccessToken;
import nest.model.nest.ClientMetadata;
import nest.model.nest.Device;
import nest.model.nest.MetaData;
import nest.model.nest.Structure;
import nest.model.nest.Thermostat;
import nest.service.UserPreferencesService;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import timber.log.Timber;

/**
 * Service to communicate with Nest backend
 */
public class NestService implements Firebase.AuthListener, ValueEventListener, Firebase.CompletionListener {

    private static final int ACTIVITY_REQUEST_CODE = 234;

    @Inject
    Context context;
    @Inject
    NestApi nestApi;
    @Inject
    Provider<UserPreferencesService> prefs;

    private WeakReference<NestListener> listenerRef;

    private final BehaviorSubject<MetaData> metadataSubject = BehaviorSubject.create();
    private final BehaviorSubject<Structure> structureSubject = BehaviorSubject.create();
    private final BehaviorSubject<Thermostat> thermostatSubject = BehaviorSubject.create();

    private boolean authenticated = false;
    private boolean busy = false;
    private boolean waitingToken = false;

    private volatile Firebase firebase;

    public NestService() {
        App.getGraph().inject(this);
    }

    /**
     * Authenticate on Nest backend. First of all, should obtain an access token.
     */
    public void login() {
        if (waitingToken || isAuthenticated()) {
            return;
        }
        final AccessToken token = prefs.get().getNestToken();
        if (token == null) {
            Timber.v("Token is empty");
            waitingToken = true;
            obtainAccessToken();
        } else {
            authenticate(token);
        }
    }

    /**
     * Close and forget session
     */
    public void logout() {
        waitingToken = false;
        unsubscribeUpdates();
        if (isAuthenticated()) {
            getFirebase().unauth((error, fb) -> {
                if (error == null) {
                    setAuthenticated(false);
                    prefs.get().setNestToken(null);
                    final NestListener listener = getListener();
                    if (listener != null) {
                        listener.onLoggedOut();
                    }
                }
            });
        }
    }

    /**
     * Start to listen for updates from backend
     * @param listener    Listener should be able to help service to obtain access token
     */
    public void start(NestListener listener) {
        listenerRef = new WeakReference<>(listener);
        if (!busy && checkAuth()) {
            Timber.v("Starting...");
            subscribeUpdates();
        }
    }

    /**
     * Stop listen to backend updates, if listener is used to start
     * @param listener    Listener should be the same that started listening
     */
    public void stop(NestListener listener) {
        if (isAuthenticated() && getListener() == listener) {
            Timber.v("Stopping...");
            listenerRef = null;
            unsubscribeUpdates();
        }
    }

    /**
     * Bypass activity result to service. Nest can emit token only through web page. So let's use different Activity for that.
     * @param requestCode    Request code originally supplied to start activity
     * @param resultCode     Activity's operation result code
     * @param intent         Result Intent
     * @return true if result was handled properly
     */
    public boolean onActivityResult(int requestCode, int resultCode, Intent intent) {
        final AccessToken token;
        if (requestCode == ACTIVITY_REQUEST_CODE) {
            waitingToken = false;
            if (resultCode == Activity.RESULT_OK && (token = NestAuthActivity.getAccessToken(intent)) != null) {
                busy = true;
                Timber.v("Got auth token: " + token.token + " expires: " + token.expiresIn);
                prefs.get().setNestToken(token);
                authenticate(token);
            } else {
                Timber.e("Unable to resolve access token from payload.");
                setAuthenticated(false);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onAuthSuccess(Object o) {
        busy = false;
        Timber.v("Authentication succeeded.");
        setAuthenticated(true);
        subscribeUpdates();
    }

    @Override
    public void onAuthError(FirebaseError firebaseError) {
        busy = false;
        Timber.v("Authentication failed with error: " + firebaseError.toString());
        setAuthenticated(false);
    }

    @Override
    public void onAuthRevoked(FirebaseError firebaseError) {
        busy = false;
        Timber.v("Authentication revoked with error: " + firebaseError.toString());
        setAuthenticated(false);
        login();
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        Observable
                .<Boolean>create(subscriber -> {
                    final Map<String, Object> values = dataSnapshot.getValue(StringObjectMapIndicator.INSTANCE);
                    for (Map.Entry<String, Object> entry : values.entrySet()) {
                        notifyUpdateListeners(entry);
                    }
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                })
                .subscribeOn(Schedulers.computation())
                .subscribe(value -> {}, error -> Timber.e(error, "Parse error"));
    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {
        Timber.e("Cancelled: %s", firebaseError);
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public Observable<MetaData> getMetaDataObservable() {
        return metadataSubject;
    }

    public Observable<Structure> getStructureObservable() {
        return structureSubject;
    }

    public Observable<Thermostat> getThermostatObservable() {
        return thermostatSubject;
    }

    @WorkerThread
    private void notifyUpdateListeners(Map.Entry<String, Object> entry) {
        final Map<String, Object> value = (Map<String, Object>) entry.getValue();
        switch (entry.getKey()) {
            case MetaData.METADATA:
                updateMetaData(value);
                break;
            case Device.DEVICES:
                updateDevices(value);
                break;
            case Structure.STRUCTURES:
                updateStructures(value);
                break;
        }
    }

    @WorkerThread
    private void updateMetaData(Map<String, Object> metadataMap) {
        final MetaData metaData = parse(metadataMap, MetaData.class);
        if (metaData != null) {
            Timber.v("Update: metadata");
            metadataSubject.onNext(metaData);
        }
    }

    @WorkerThread
    private void updateDevices(Map<String, Object> devices) {
        for (Map.Entry<String, Object> entry : devices.entrySet()) {
            final Map<String, Object> value = (Map<String, Object>) entry.getValue();
            switch (entry.getKey()) {
                case Thermostat.THERMOSTATS:
                    updateThermostats(value);
                    break;
            }
        }
    }

    @WorkerThread
    private void updateThermostats(Map<String, Object> thermostatsMap) {
        for (Map.Entry<String, Object> entry : thermostatsMap.entrySet()) {
            final Map<String, Object> value = (Map<String, Object>) entry.getValue();
            final Thermostat thermostat = parse(value, Thermostat.class);
            if (thermostat != null) {
                Timber.v("Update: thermostat");
                thermostatSubject.onNext(thermostat);
            }
        }
    }

    @WorkerThread
    private void updateStructures(Map<String, Object> structures) {
        for (Map.Entry<String, Object> entry : structures.entrySet()) {
            final Map<String, Object> value = (Map<String, Object>) entry.getValue();
            final Structure structure = parse(value, Structure.class);
            if (structure != null) {
                Timber.v("Update: structure");
                structureSubject.onNext(structure);
            }
        }
    }

    private NestListener getListener() {
        return listenerRef == null ? null : listenerRef.get();
    }

    private boolean checkAuth() {
        if (isAuthenticated()) {
            return true;
        } else {
            login();
            return false;
        }
    }

    private void obtainAccessToken() {
        Timber.v("Starting auth flow...");
        final ClientMetadata data = new ClientMetadata(BuildConfig.NEST_CLIENT_ID, BuildConfig.NEST_CLIENT_SECRET, BuildConfig.NEST_REDIRECT_URL);
        final NestListener listener;
        if ((listener = getListener()) != null) {
            listener.onStartActivityForResult(ACTIVITY_REQUEST_CODE, data);
        }
    }

    private void authenticate(AccessToken token) {
        Timber.v("Authenticating...");
        getFirebase().auth(token.token, this);
    }

    private void setAuthenticated(boolean value) {
        if (authenticated != value) {
            authenticated = value;

            final NestListener listener;
            if ((listener = getListener()) != null) {
                listener.onAuthStatusChanged(value);
            }
        }
    }

    private void subscribeUpdates(){
        Timber.v("Subscribing to updates...");
        getFirebase().addValueEventListener(this);
    }

    private void unsubscribeUpdates() {
        Timber.v("Unsubscribing from updates...");
        getFirebase().removeEventListener(this);
    }

    public void sendRequest(String path, Object value) {
        Observable
                .create(subscriber -> {
                    getFirebase().child(path).setValue(value, this);
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                })
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    @Override
    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
        Timber.v("Request completed, error: %s", firebaseError);
    }

    private Firebase getFirebase() {
        if (firebase == null) {
            synchronized (this) {
                if (firebase == null) {
                    Firebase.goOffline();
                    Firebase.goOnline();
                    Config defaultConfig = Firebase.getDefaultConfig();
                    defaultConfig.setLogLevel(Logger.Level.DEBUG);
                    final Firebase f = new Firebase(NestApi.FIREBASE_NEST_URL);
                    firebase = f;
                }
            }
        }

        return firebase;
    }

    private static <T> T parse(Map<String, Object> map, Class<T> clazz) {
        try {
            return LoganSquare.parse(new JSONObject(map).toString(), clazz);
        } catch (Exception ex) {
            Timber.e(ex, "Unable to parse object");
        }
        return null;
    }

    // Marker for Firebase to retrieve a strongly-typed collection
    private static class StringObjectMapIndicator extends GenericTypeIndicator<Map<String, Object>> {

        static final StringObjectMapIndicator INSTANCE = new StringObjectMapIndicator();
    }
}
