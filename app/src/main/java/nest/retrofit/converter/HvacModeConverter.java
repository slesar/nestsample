package nest.retrofit.converter;

import com.bluelinelabs.logansquare.typeconverters.StringBasedTypeConverter;

import nest.model.nest.HvacMode;

public class HvacModeConverter extends StringBasedTypeConverter<HvacMode> {

    @Override
    public HvacMode getFromString(String string) {
        return HvacMode.from(string);
    }

    @Override
    public String convertToString(HvacMode mode) {
        return mode.getKey();
    }
}
