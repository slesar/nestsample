package nest.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import javax.inject.Inject;

import nest.App;
import nest.R;
import nest.model.EmptyBody;
import nest.model.nest.AccessToken;
import nest.model.nest.ClientMetadata;
import nest.service.nest.NestApi;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class NestAuthActivity extends AppCompatActivity {

    public static boolean hasEmptyArgs(String ... args) {
        for (int i = 0; i < args.length; i++) {
            if (TextUtils.isEmpty(args[i])) {
                return true;
            }
        }
        return false;
    }

    private static final String ARGS_CLIENT_METADATA = "ARGS_CLIENT_METADATA";
    private static final String ARGS_ACCESS_TOKEN = "ARGS_ACCESS_TOKEN";

    public static AccessToken getAccessToken(Intent intent) {
        return intent != null && intent.hasExtra(ARGS_ACCESS_TOKEN) ? intent.getParcelableExtra(ARGS_ACCESS_TOKEN) : null;
    }

    public static void requestAccessToken(Activity activity, int requestCode, ClientMetadata data) {
        final Intent authFlowIntent = new Intent(activity, NestAuthActivity.class);
        authFlowIntent.putExtra(ARGS_CLIENT_METADATA, data);
        activity.startActivityForResult(authFlowIntent, requestCode);
    }

    @Nullable
    private static String parseCode(String url) {
        try {
            return Uri.parse(url).getQueryParameter("code");
        } catch (Exception e) {
            return null;
        }
    }

    @Inject
    NestApi nestApi;

    private WebView webView;
    private ProgressBar progressBar;

    private ClientMetadata clientMetadata;

    private String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getGraph().inject(this);

        setContentView(R.layout.activity_nest_auth);

        webView = (WebView) findViewById(R.id.activity_webview);
        progressBar = (ProgressBar) findViewById(R.id.activity_progress);

        showProgress();

        parseArgs();
    }

    private void parseArgs() {
        Observable
                .<ClientMetadata>create(subscriber -> {
                    final ClientMetadata metadata = getClientMetadata();
                    if (hasEmptyArgs(metadata.getRedirectURL(), metadata.getClientID(), metadata.getStateValue())) {
                        Timber.e("Got invalid arguments");
                        subscriber.onNext(null);
                    }
                    subscriber.onNext(metadata);
                    subscriber.onCompleted();
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(metadata -> {
                    if (metadata == null) {
                        finish(null);
                    } else {
                        clientMetadata = metadata;
                        showLoginPage();
                    }
                });
    }

    private void finish(@Nullable AccessToken token) {
        if (token == null) {
            setResult(RESULT_CANCELED);
        } else {
            final Intent intent = new Intent();
            intent.putExtra(ARGS_ACCESS_TOKEN, token);
            setResult(RESULT_OK, intent);
        }
        finish();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void showLoginPage() {
        webView.setWebChromeClient(new ProgressChromeClient());
        webView.setWebViewClient(new RedirectClient());
        webView.getSettings().setJavaScriptEnabled(true);

        final String url = String.format(NestApi.REQUEST_CLIENT_CODE_URL, clientMetadata.getClientID(), clientMetadata.getStateValue());
        Timber.v("Loading url: " + url);
        webView.loadUrl(url);
    }

    @WorkerThread
    private ClientMetadata getClientMetadata() {
        final Intent intent = getIntent();
        return intent == null ? null : intent.getParcelableExtra(ARGS_CLIENT_METADATA);
    }

    void requestAccessToken() {
        showProgress();
        nestApi.getAccessToken(code, clientMetadata.getClientID(), clientMetadata.getClientSecret(), new EmptyBody())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onAccessTokenCompleted, this::onAccessTokenFailed);
    }

    private void onAccessTokenCompleted(AccessToken token) {
        Timber.v("Got access token: %s", token);
        if (token != null) {
            finish(token);
        } else {
            onAccessTokenFailed(null);
        }
    }

    private void onAccessTokenFailed(Throwable t) {
        Timber.e("Unable to get access token: %s", t);
        finish(null);
    }

    private void showProgress() {
        webView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        webView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private class ProgressChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            final int vis = progressBar.getVisibility();
            if (newProgress < 100 && vis != View.VISIBLE) {
                showProgress();
            } else if (newProgress == 100 && vis != View.GONE) {
                hideProgress();
            }
        }
    }

    private class RedirectClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (!url.startsWith(clientMetadata.getRedirectURL())) {
                code = null;
                return false;
            }
            code = parseCode(url);
            Timber.v("Got code: %s", code);
            if (TextUtils.isEmpty(code)) {
                finish(null);
                return true;
            }
            requestAccessToken();
            return true;
        }
    }
}
