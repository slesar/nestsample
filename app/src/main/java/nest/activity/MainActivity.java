package nest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import javax.inject.Inject;

import nest.App;
import nest.R;
import nest.model.nest.AwayState;
import nest.model.nest.ClientMetadata;
import nest.service.nest.NestListener;
import nest.service.nest.NestService;
import nest.ui.holder.StructureViewHolder;
import nest.ui.holder.ThermostatViewHolder;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

public class MainActivity extends AppCompatActivity implements NestListener {

    @Inject
    NestService nestService;

    private StructureViewHolder structureViewHolder;
    private ThermostatViewHolder thermostatViewHolder;

    private CompositeSubscription subscriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.getGraph().inject(this);

        setContentView(R.layout.activity_main);

        final View content = findViewById(R.id.activity_content);

        structureViewHolder = new StructureViewHolder(content);

        thermostatViewHolder = new ThermostatViewHolder(content);
        thermostatViewHolder.setEnabled(nestService.isAuthenticated());
        thermostatViewHolder.showProgress();
    }

    @Override
    protected void onStart() {
        super.onStart();
        nestService.start(this);

        final Subscription structureSub = nestService.getStructureObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((structure) -> {
                    structureViewHolder.updateStructure(structure);
                    thermostatViewHolder.setAwayState(structure.awayState);
                });
        manageSubscription(structureSub);

        final Subscription thermostatSub = nestService.getThermostatObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(thermostatViewHolder::updateThermostat);
        manageSubscription(thermostatSub);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (subscriptions != null) {
            subscriptions.clear();
            subscriptions = null;
        }
        nestService.stop(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == RESULT_OK) {
            nestService.onActivityResult(requestCode, resultCode, intent);
        }
    }

    @Override
    public void onStartActivityForResult(int code, ClientMetadata data) {
        NestAuthActivity.requestAccessToken(this, code, data);
    }

    @Override
    public void onAuthStatusChanged(boolean isAuth) {
        thermostatViewHolder.setEnabled(isAuth);
    }

    @Override
    public void onLoggedOut() {
        finish();
    }

    protected void manageSubscription(Subscription sub) {
        if (subscriptions == null) {
            subscriptions = new CompositeSubscription();
        }
        subscriptions.add(sub);
    }
}
