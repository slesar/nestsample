package nest.model.nest;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Random;

public class ClientMetadata implements Parcelable {

    private final String clientID;
    private final String stateValue;
    private final String clientSecret;
    private final String redirectURL;

    public ClientMetadata(String clientID, String clientSecret, String redirectURL) {
        this.clientID = clientID;
        // TODO validate
        this.stateValue = "app-state" + System.nanoTime() + "-" + new Random().nextInt();
        this.clientSecret = clientSecret;
        this.redirectURL = redirectURL;
    }

    ClientMetadata(Parcel in) {
        clientID = in.readString();
        stateValue = in.readString();
        clientSecret = in.readString();
        redirectURL = in.readString();
    }

    public String getClientID() {
        return clientID;
    }

    public String getStateValue() {
        return stateValue;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(clientID);
        dest.writeString(stateValue);
        dest.writeString(clientSecret);
        dest.writeString(redirectURL);
    }

    public static final Parcelable.Creator<ClientMetadata> CREATOR = new Parcelable.Creator<ClientMetadata>() {

        @Override
        public ClientMetadata createFromParcel(Parcel source) {
            return new ClientMetadata(source);
        }

        @Override
        public ClientMetadata[] newArray(int size) {
            return new ClientMetadata[size];
        }
    };
}
