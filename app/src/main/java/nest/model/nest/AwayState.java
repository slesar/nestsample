package nest.model.nest;


public enum AwayState {

    /** State when the user has explicitly set the structure to "Away" state. */
    AWAY("away"),

    /** State when Nest has detected that the structure is not occupied. */
    AUTO_AWAY("auto-away"),

    /** State when the user is at home */
    HOME("home"),

    /** State for when the home/away status is unknown */
    UNKNOWN("unknown");

    public static AwayState from(String key) {
        final AwayState[] values = values();
        for (int i = 0 ; i < values.length; i++) {
            if (values[i].key.equalsIgnoreCase(key)) {
                return values[i];
            }
        }
        return UNKNOWN;
    }

    private final String key;

    AwayState(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
