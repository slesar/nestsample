package nest.model.nest;

import android.support.annotation.StringDef;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import nest.retrofit.converter.HvacModeConverter;

@JsonObject
public class Thermostat extends Device {

    public static final String THERMOSTATS = "thermostats";

    public static final String FIELD_CAN_COOL = "can_cool";
    public static final String FIELD_CAN_HEAT = "can_heat";
    public static final String FIELD_EMERGENCY_HEAT = "is_using_emergency_heat";
    public static final String FIELD_HAS_FAN = "has_fan";
    public static final String FIELD_HUMIDITY = "humidity";
    public static final String FIELD_FAN_TIMEOUT = "fan_timer_timeout";
    public static final String FIELD_HAS_LEAF = "has_leaf";
    public static final String FIELD_TEMP_SCALE = "temperature_scale";
    public static final String FIELD_AWAY_TEMP_HIGH_F = "away_temperature_high_f";
    public static final String FIELD_AWAY_TEMP_HIGH_C = "away_temperature_high_c";
    public static final String FIELD_AWAY_TEMP_LOW_F = "away_temperature_low_f";
    public static final String FIELD_AWAY_TEMP_LOW_C = "away_temperature_low_c";
    public static final String FIELD_AMBIENT_TEMP_F = "ambient_temperature_f";
    public static final String FIELD_AMBIENT_TEMP_C = "ambient_temperature_c";
    public static final String FIELD_FAN_TIMER_ACTIVE = "fan_timer_active";
    public static final String FIELD_TARGET_TEMP_F = "target_temperature_f";
    public static final String FIELD_TARGET_TEMP_C = "target_temperature_c";
    public static final String FIELD_TARGET_TEMP_HIGH_F = "target_temperature_high_f";
    public static final String FIELD_TARGET_TEMP_HIGH_C = "target_temperature_high_c";
    public static final String FIELD_TARGET_TEMP_LOW_F = "target_temperature_low_f";
    public static final String FIELD_TARGET_TEMP_LOW_C = "target_temperature_low_c";
    public static final String FIELD_HVAC_MODE = "hvac_mode";

    @StringDef({FIELD_FAN_TIMER_ACTIVE, FIELD_TARGET_TEMP_F, FIELD_TARGET_TEMP_C, FIELD_TARGET_TEMP_HIGH_F, FIELD_TARGET_TEMP_HIGH_C,
            FIELD_TARGET_TEMP_LOW_F, FIELD_TARGET_TEMP_LOW_C, FIELD_AWAY_TEMP_HIGH_F, FIELD_AWAY_TEMP_HIGH_C, FIELD_AWAY_TEMP_LOW_F,
            FIELD_AWAY_TEMP_LOW_C, FIELD_HVAC_MODE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FieldName {
    }

    @JsonField(name = FIELD_CAN_COOL)
    public boolean canCool;

    @JsonField(name = FIELD_CAN_HEAT)
    public boolean canHeat;

    @JsonField(name = FIELD_EMERGENCY_HEAT)
    public boolean isUsingEmergencyHeat;

    @JsonField(name = FIELD_HAS_FAN)
    public boolean hasFan;

    @JsonField(name = FIELD_HUMIDITY)
    public int humidity;

    @JsonField(name = FIELD_FAN_TIMEOUT)
    public String fanTimerTimeout;

    @JsonField(name = FIELD_HAS_LEAF)
    public boolean hasLeaf;

    @JsonField(name = FIELD_TEMP_SCALE)
    public String temperatureScale;

    @JsonField(name = FIELD_AWAY_TEMP_HIGH_F)
    public long awayTemperatureHighF;

    @JsonField(name = FIELD_AWAY_TEMP_HIGH_C)
    public double awayTemperatureHighC;

    @JsonField(name = FIELD_AWAY_TEMP_LOW_F)
    public long awayTemperatureLowF;

    @JsonField(name = FIELD_AWAY_TEMP_LOW_C)
    public double awayTemperatureLowC;

    @JsonField(name = FIELD_AMBIENT_TEMP_F)
    public long ambientTemperatureF;

    @JsonField(name = FIELD_AMBIENT_TEMP_C)
    public double ambientTemperatureC;

    @JsonField(name = FIELD_FAN_TIMER_ACTIVE)
    public boolean fanTimerActive;

    @JsonField(name = FIELD_TARGET_TEMP_F)
    public long targetTemperatureF;

    @JsonField(name = FIELD_TARGET_TEMP_C)
    public double targetTemperatureC;

    @JsonField(name = FIELD_TARGET_TEMP_HIGH_F)
    public long targetTemperatureHighF;

    @JsonField(name = FIELD_TARGET_TEMP_HIGH_C)
    public double targetTemperatureHighC;

    @JsonField(name = FIELD_TARGET_TEMP_LOW_F)
    public long targetTemperatureLowF;

    @JsonField(name = FIELD_TARGET_TEMP_LOW_C)
    public double targetTemperatureLowC;

    @JsonField(name = FIELD_HVAC_MODE, typeConverter = HvacModeConverter.class)
    public HvacMode hvacMode;

    public Thermostat() {
    }

    @Override
    public String getPath(@FieldName String field) {
        return super.getPath(field);
    }

    @Override
    protected String getDeviceName() {
        return THERMOSTATS;
    }
}
