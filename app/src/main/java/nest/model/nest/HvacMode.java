package nest.model.nest;

public enum HvacMode {

    HEAT("heat"),
    COOL("cool"),
    HEAT_AND_COOL("heat-cool"),
    OFF("off"),
    UNKNOWN("unknown");

    public static HvacMode from(String key) {
        final HvacMode[] values = values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].key.equalsIgnoreCase(key)) {
                return values[i];
            }
        }
        return UNKNOWN;
    }

    private final String key;

    HvacMode(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
