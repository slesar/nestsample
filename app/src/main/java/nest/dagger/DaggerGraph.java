package nest.dagger;

import javax.inject.Singleton;

import dagger.Component;
import nest.dagger.module.MainModule;

@Singleton
@Component(modules = {MainModule.class})
public interface DaggerGraph extends Graph {
}
