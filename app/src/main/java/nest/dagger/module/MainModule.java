package nest.dagger.module;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nest.App;
import nest.service.UserPreferencesService;
import nest.service.nest.NestApi;
import nest.service.nest.NestApiAdapter;
import nest.service.nest.NestService;

@Module
public class MainModule {

    private final App app;

    public MainModule(App application) {
        app = application;
    }

    @Provides
    App provideApp() {
        return app;
    }

    @Provides
    Context provideContext() {
        return app.getApplicationContext();
    }

    @Provides
    OkHttpClient provideHttpClient() {
        return new OkHttpClient();
    }

    @Provides
    @Singleton
    NestApi provideNestApi() {
        return new NestApiAdapter();
    }

    @Provides
    UserPreferencesService provideUserPreferencesService() {
        return new UserPreferencesService(app);
    }

    @Provides
    @Singleton
    NestService provideNestService() {
        return new NestService();
    }
}
