package nest.dagger;

import nest.App;
import nest.activity.MainActivity;
import nest.activity.NestAuthActivity;
import nest.dagger.module.MainModule;
import nest.service.nest.NestService;
import nest.ui.holder.StructureViewHolder;
import nest.ui.holder.ThermostatViewHolder;

public interface Graph {

    // Application
    void inject(App app);

    // Activity
    void inject(MainActivity mainActivity);
    void inject(NestAuthActivity nestAuthActivity);

    // Service
    void inject(NestService nestService);

    // Other
    void inject(StructureViewHolder structureViewHolder);
    void inject(ThermostatViewHolder thermostatViewHolder);

    final class Initializer {
        private Initializer() {
        } // No instances.

        public static DaggerGraph init(App app) {
            return DaggerDaggerGraph.builder()
                    .mainModule(new MainModule(app))
                    .build();
        }
    }
}
